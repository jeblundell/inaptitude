#!/usr/bin/perl -w

#    Inaptitude v0.1
#    Because fuck Debian's inability to revert an upgrade
#    Copyright (C) 2014 Jim Blundell
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

use strict;
use Date::Parse;

my ($cache) = "/var/cache/apt/archives";
my ($log)  = "/var/log/apt/history.log";

my (@res, @sess, @acts, @cmds, @fail);
my (@app, @sel);
my ($cmd, $scmd, $conf, $test, $maxts);
my ($ldate, $ltime, $lts, $lstr, $lact);
my ($c, $m, $n, $s, $i, $f, $la);

if (scalar(@ARGV) < 1) {
	print <<EOF

Syntax: inaptitude.pl <action> [<parameters>]

      show                    Show activity (to find reversion point)
      revert last             Revert last upgrade
      revert <ID>             Revert a specific session

Example:

      inaptitude.pl show
      inaptitude.pl revert last

EOF
;
	exit;
}

if (! -f $log) {
	print "The logfile $log does not exist\n";
	exit;
}
$cmd = shift(@ARGV);

open LOG, "< $log";
while (<LOG>) {
	$_ =~ s/[\r\n]//g;
	if ($_ =~ /^Start-Date: ([^ ]+) +([^ ]+)/) {
		$ldate = $1;
		$ltime = $2;
		$lts = str2time($ldate." ".$ltime);
		push @sess, {'id' => $lts, 'date' => $ldate, 'time' => $ltime};
	} elsif ($_ =~ /^([^:]*): (.*)/) {
		$lact = lc($1);
		$lstr  = $2;
		if ($lact eq 'install' || $lact eq 'upgrade' || $lact eq 'remove') {
			@app = &parse_pkgs ($lstr, $lact, $lts);
			push @res, @app;
		}
	}
}
close LOG;

if ($cmd eq 'show') {
	foreach $s (@sess) {
		$m = 0;
		foreach $a ("install", "upgrade", "remove") {
			$n = 0;
			foreach $i (@res) {
				if ($i->{'id'} == $s->{'id'} && $i->{'action'} eq $a) {
					$m++; $n++;
					if ($m == 1) { printf "Session %s %s (ID %d)\n", $s->{'date'}, $s->{'time'}, $s->{'id'}; }
					if ($n == 1) { print "  $a\n"; }
					printf "    %-35s (%s -> %s)\n", $i->{'name'}, $i->{'old'}, $i->{'new'};
				
				}
			}
			if ($n > 0) { printf "    (%d total $a)\n", $n; }
		}
		if ($m > 0 && $m != $n) { printf " (%d total)\n", $m; }
	}
} elsif ($cmd eq 'revert' || $cmd eq 'revert-force') {
	$scmd = shift(@ARGV);

	@sel = ();
	if ($scmd eq 'last') {
		$maxts = 0;
		foreach $s (@sess) {
			foreach $i (@res) {
				if ($i->{'id'} == $s->{'id'} && $i->{'action'} eq "upgrade") {
					if ($s->{'id'} > $maxts) {
						$maxts = $s->{'id'};
						last;
					}
				}
			}
		}
		if ($maxts == 0) {
			print "No upgrades in log specified\n";
			exit;
		}
		push @sel, $maxts;
	} else {
		$n = int($scmd);
		foreach $s (@sess) {
			if ($s->{'id'} == $n) {
				push @sel, $n;
				last;
			}
		}
		if (scalar(@sel) == 0) {
			print "No session with ID $n found\n";
			exit;
		}
	}

	#@sel is an array, just in case future version implements several reversions in one
	@sel = sort {$b <=> $a} @sel; #Reverse sort (from perlfunc)

	@acts = ();
	foreach $n (@sel) {
		print ">>> Constructing reversion plan for apt log entry $n\n";
		foreach $i (@res) {
			if ($i->{'id'} == $n) {
				if ($i->{'action'} eq "remove" || $i->{'action'} eq "upgrade") {
					if (!(&package_current ($i->{'name'}, $i->{'old'}, $i->{'arch'}))) {
						push @acts, {
							"action"  => "install",
							"name"    => $i->{'name'},
							"version" => $i->{'old'},
							"arch"    => $i->{'arch'},
							"pkg"     => &find_package($i->{'name'}, $i->{'old'}, $i->{'arch'})
						};
					}
				} elsif ($i->{'action'} eq "install") {
				#	push @acts, {
				#		"action"  => "remove",
				#		"name"    => $i->{'name'},
				#		"version" => $i->{'old'},
				#	};
				} else {
					#Shouldn't happen...
					printf "Don't know how to handle action %s; aborting!\n", $i->{'action'}, $i->{'name'};
					exit;
				}
			}
		}
	}
	
	print "\n";
	print ">>> Reversion plan\n";
	$la = ""; $m = $n = 0;
	foreach $a (@acts) {
		if ($a->{'action'} ne $la) {
			$la = $a->{'action'};
			if ($n > 0) { printf "   (%d total)\n", $n; }
			print "   $la\n";
			$n = 0;
		}
		$m++; $n++;
		printf "     %-35s %s\n", $a->{'name'}, $a->{'version'};
	}
	if ($n > 0) { printf "   (%d total)\n", $n; }
	if ($m != $n) { printf "(%d total)\n", $m; }

	print "\n";
	chdir $cache;
	@fail = ();
	foreach $a (@acts) {
		if ($a->{'action'} eq 'install' && $a->{'pkg'} eq '') {
			printf ">>>Downloading %s\n", $a->{'name'};
			system sprintf("apt-get download -a %s %s=%s", $a->{'arch'}, $a->{'name'}, $a->{'version'});
			$a->{'pkg'} = &find_package ($a->{'name'}, $a->{'version'}, $a->{'arch'});
			if ($a->{'pkg'} eq '') {
				push @fail, sprintf("%s_%s_%s.deb", $a->{'name'}, $a->{'version'}, $a->{'arch'});
			}
		}
	}

	if (scalar(@fail) > 0) {
		print "\nFailed to locate some packages, revert aborted\n\n";
		print "You need to obtain the following packages manually and store them in $cache:\n";
		foreach $f (@fail) {
			print "$f\n";
		}
		print "\n";
		if ($cmd ne 'revert-force') {
			print "If you really don't mind clearing up a mess, you can try 'revert-force' to go ahead anyway :)\n\n";
			exit;
		}
	}

	@cmds = ();
	foreach $a (@acts) {
		if ($a->{'action'} eq 'remove') {
			push @cmds, "dpkg -r ".$a->{'name'};
		} elsif ($a->{'action'} eq 'install') {
			if ($a->{'pkg'} ne "") {
				push @cmds, "dpkg -i ".$a->{'pkg'};
			}
		}
	}

	if (scalar(@cmds) == 0) {
		if (scalar(@fail) == 0) {
			print "\nReversion has already been done.\n\n";
		} else {
			print "\nNo commands to execute; you need to manually install the failed items.\n\n";
		}
		exit;
	}

	print ">>>Commands to execute\n";
	foreach $c (@cmds) {
		print "$c\n";
	}
	print ">>>\n";

	print <<EOF

Ready to attempt reversion. The commands above will be executed.

In theory if this goes horribly wonky, you can theoretically revert
this reversion (good God, no!). Personally, at that point, I wouldn't 
and I would just opt for doing the whole lot manually :) either way, 
please make sure everything's backed up properly

EOF
;

	$conf = "REVERT";
	print "Please enter $conf to confirm proceeding\n";
	$test = <STDIN>;
	if (!($test)) { exit; }
	$test =~ s/[\r\n]//g;
	print "$test\n";
	if ($test ne $conf) { exit; }

	print "\n\n";

	foreach $c (@cmds) {
		print "\n>>> $c\n";
		system "$c";
	}

	system "dpkg --configure --pending";

	print "\n\nReversion complete.\n\n";
}

sub parse_pkgs {
	my ($str, $action, $sid) = @_;
	my (@out, $item);

	$str =~ s/\(([^,\)]*)\)/\($1, $1\)/g;

	#print "$str\n";
	while ($str =~ /([^:]+):([^ ]+) \(([^,]+), ([^\)]+)\)/) {
		$item = {"action" => $action,
			 "id" => $sid,
			 "name" => $1, 
			 "arch" => $2,
			 "old" => $3, 
			 "new" => $4};
		push @out, $item;

		$str = $';
		$str =~ s/^[^a-zA-Z0-9]+//g;
	}
	return @out;
}
sub find_package {
	my ($name, $ver, $arch) = @_;
	my ($a, $f);

	foreach $a ("all", $arch) {
		$f = $cache."/".$name."_".$ver."_".$a.".deb";
		if (-f $f) { return $f; }
	}
	
	return "";
}
sub package_current {
	my ($name, $ver, $arch) = @_;
	my (@dout, $a, $v, $n);

	@dout = `dpkg -s $name`;
	$a = $v = 0;
	foreach $n (@dout) {
		$n =~ s/[\r\n]//g;
		if ($n =~ /^Architecture: (.*)/) {
			if ($1 eq $arch || $1 eq 'all' || $arch eq 'all') {
				$a = 1; 
				if ($v) { last; } else { next; }
			}
		} elsif ($n =~ /^Version: (.*)/) {
			if ($1 eq $ver) {
				$v = 1; 
				if ($a) { last; } else { next; }
			}
		}
	}

	return ($a && $v);
}
